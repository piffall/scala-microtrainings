// Variable length arguments

def capitalize(args: String*): Seq[String] = args.map(_.toUpperCase)

capitalize("Tofol")

capitalize("Oscar", "Tòfol")