// Curried functions

def divideIfDividendIsGreaterThanDivisorElseRandom(a: Int, b: Int)(randomizer:() => Int): Int = {
  if (a > b) a / b
  else randomizer()
}

def randomizer() = {
  println("This run.")
  9
}

divideIfDividendIsGreaterThanDivisorElseRandom(1, 2)(randomizer)
