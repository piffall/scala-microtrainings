package caseclass {

  // Case Class
  case class Person(name: String, age: Int)

}

package regularclass {

  // Class
  class Person(name: String, age: Int) {}

  // Extended class
  class Worker(name: String, age: Int, role: String) extends Person(name, age) {}

  // Add constructors
  class MultipleConstructorsPerson(name: String, age: Int) {

    def this() = this("Unknown", 0)
    def this(name: String) = this(name, 0)
    def this(age: Int) = this("Unknown", age)

  }

  // -----------------------

  // Trait
  trait PersonPrinter {

    val nameToPrint: String
    val ageToPrint: _

    def print = {
      println(s"Hello, my name is $nameToPrint and I'm $ageToPrint")
    }

  }

  // Extend with trait
  class PrintableWorker(name: String, age: Int, role: String)
      extends Worker(name, age, role)
      with PersonPrinter {

    /**
      * Constructor
      */
    val nameToPrint = name
    val ageToPrint = age

  }

  // Object
  object Me {
    val name = "Tòfol"
  }

}
