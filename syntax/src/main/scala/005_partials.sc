// Partial functions

def multiply(a: Int, b: Int): Int = a * b

val duplicate = multiply(2, _: Int)
duplicate(3)

val triplicate = multiply(3, _: Int)
triplicate(3)