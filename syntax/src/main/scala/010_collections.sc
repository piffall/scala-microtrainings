// Collections

// Array
val a = Array(1, 2, 3)

// List
val b = List(1, 2, 3)

// Sets
val c = Set(1, 2, 3)

// Tuples
val d = (1, 2, 3, 4)

// Maps
val e = Map(
  "a" -> 1,
  "b" -> 2,
  "c" -> 3
)

// Seq
val f = Seq(5,6,7,8)


// Functionals

// Map
a.map(_.toString)

// Foreach
b.foreach((a: Int) => println(a))

// Filter
c.filter((a: Int) => a % 2 == 0)

// Zip
a.zip(b)

// Partition
a.partition(_ % 2 == 0)

// Find
a.filter(_ > 2)

// Drop & dropWhile
a.drop(2)
f.dropWhile(_ < 6)
a.dropRight(1)

// Fold left
val acc = 0
a.foldLeft(0)((acc, i) => acc + i)
a.sum

// Flatten
val x = List(List(1, 2), List(3,4)).flatten

// Flat Map
val z = List(List(1, 2), List(3,4)).flatMap(x => x.map(_ * 2))