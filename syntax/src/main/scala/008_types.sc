// Types

type Id = Int
type Coordinates = (Double, Double)
type Direction = String
type Route = List[Direction]
