// Values cannot change

val two = 1 + 1

// Variables can change

var three = two
three = two + 1