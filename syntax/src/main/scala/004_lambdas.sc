// Anonymous functions

val duplicate = (a: Int) => a * 2
duplicate(40)

def applyMyFunc(b: Int)(c:Int => Int): Int = {
  c(b)
}

def triplicate(a: Int): Int = a * 3

applyMyFunc(3)(duplicate)
applyMyFunc(3)(triplicate)
applyMyFunc(4){
  (a: Int) =>
    a * 10
}
applyMyFunc(3)(_ * 10)
