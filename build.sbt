name := "scala-microtrainings"

version := "1.0"

scalaVersion := "2.11.12"

lazy val root = project.in(file("."))

lazy val syntax = project.in(file("syntax"))

lazy val implicits = project.in(file("implicits"))

lazy val `pattern-matching` = project.in(file("pattern-matching"))

lazy val `scalac-magics` = project.in(file("scalac-magics"))
