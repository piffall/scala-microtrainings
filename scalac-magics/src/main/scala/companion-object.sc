package magic {

  case class Person(name: String, lastname: String)

}

package muggle {

  object Person {

    def apply(name: String, lastname: String): Person =
      new Person(name, lastname)

    def unapply(p: Person): Option[(String, String)] = Some((p.name, p.lastname))

  }

  class Person(val name: String, val lastname: String) {

  }
}