import scala.util.Random

val x = Random.nextInt(10)

x match {
  case 1 => "One"
  case 2 => "Two"
  case _ => "Too many"
}