// On iterations
val stringList =
  Seq(Some("Oscar"),
    None,
    Some("Joan"))

stringList foreach {
  case Some(name) => println(name)
  case None => println("None")
}

// For getter
def getUser(id: Int): Option[String] = {

  // users map pretends to be some kind of DDBB
  val users = Map(
    1 -> "Oscar",
    2 -> "Dani",
    3 -> "David"
  )

  // Return if exists, else return None
  if (users.contains(id)) {
    Some(users(id))
  } else {
    None
  }
}

// Pass as arguments
def printString(person: Option[String]): Unit =
  person match {
    case Some(name) => println(name)
    case None => println("None")
  }

printString(getUser(3))