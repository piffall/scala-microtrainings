
trait Person {
  def greet: Unit = ???
}

case object Unknown extends Person

case class Known(name: String) extends Person {
  override def greet = println("Hello " + name + ", how are you?")
}

case class Friend(name: String, yearsSinceFriends: Int) extends Person {
  override def greet = println("Wasup " + name + "?")
}

// This will work
val oscar = Friend("Oscar", 2)
oscar.greet

// This will fail
val someOne = Unknown
try {
  someOne.greet
} catch {
  case ex: NotImplementedError =>
    println("Ups, sorry! I thought you were someone else")
}