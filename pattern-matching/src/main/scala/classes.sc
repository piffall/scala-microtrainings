import scala.util.Random

trait Person
case object Unknown extends Person
case class Known(name: String) extends Person
case class Friend(name: String, yearsSinceFriends: Int) extends Person

val persons: Array[Person] = Array(
  Friend("Oscar", 2),
  Friend("Anto", 1),
  Friend("Joan", 1),
  Known("Bartolomé"),
  Unknown)

persons(Random.nextInt(5)) match {
  case Friend(name, years) => name + " is my friend since " + years + " ago"
  case Known(name) => "I know " + name
  case Unknown => "I don't know who is he/she"
}
