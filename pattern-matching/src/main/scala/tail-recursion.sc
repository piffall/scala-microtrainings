
def sum(seq: Seq[Int]): Int = {
  def sum(acc: Int, seq: Seq[Int]): Int = {
    seq match {
      case a :: Nil => acc
      case x :: xs => sum(acc + x, xs)
    }
  }
  sum(0, seq)
}

val seq = Seq(1, 2, 3, 4, 5, 6, 7, 8, 9, 10)
sum(seq)
