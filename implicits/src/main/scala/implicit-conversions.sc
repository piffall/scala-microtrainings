trait Person
case object Unknown extends Person
final case class Known(name: String) extends Person

implicit case class Terrestrian(person: Known) {
  def greet(): Unit = println("Greetings " + person.name + " from Earth")
}

//implicit def personToEmployee(person: Known): Terrestrian = Terrestrian(person)

val person = Known("Juan")
person.greet()