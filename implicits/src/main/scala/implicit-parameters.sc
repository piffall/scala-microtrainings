trait Person
case object Unknown extends Person
case class Known(name: String) extends Person

def eKill(implicit known: Known): Person = {
  println("Hereinafter " + known.name + " is completly e-death" )
  Unknown
}

implicit val oscar = Known("Oscar")
val joan = Known("Joan")
val noOne = eKill

eKill(joan)