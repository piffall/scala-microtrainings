# scala-microtrainings

This project offers some examples of code for developers how are introducing 
to scala.

## What does it cover?

* Basic syntax ✔
* Some scalac magics ✔
* Traits ✔
* Case classes ✔
* Implicits ✔
* Pattern matching ✔
* Options ✔

## License

Please, see [LICENSE.txt](LICENSE.txt)

## Author

Cristòfol Torrens | BigData Developer